#NAME nat_def.

Nat1 : fol.sort.

ADD1 : fol.function.
[] fol.fun_domain ADD1 --> fol.read_sorts (nat.S (nat.S nat.O)) Nat1 Nat1.
[] fol.fun_codomain ADD1 --> Nat1.

def add1 := fol.fun_apply ADD1.

LEQ1 : fol.predicate.
[] fol.pred_domain LEQ1 --> fol.read_sorts (nat.S (nat.S nat.O)) Nat1 Nat1.

def leq1 := fol.pred_apply LEQ1.

Nat2 : fol.sort.

ADD2 : fol.function.
[] fol.fun_domain ADD2 --> fol.read_sorts (nat.S (nat.S nat.O)) Nat2 Nat2.
[] fol.fun_codomain ADD2 --> Nat2.

def add2 := fol.fun_apply ADD2.

LEQ2 : fol.predicate.
[] fol.pred_domain LEQ2 --> fol.read_sorts (nat.S (nat.S nat.O)) Nat2 Nat2.

def leq2 := fol.pred_apply LEQ2.

REL : fol.predicate.
[] fol.pred_domain REL --> fol.read_sorts (nat.S (nat.S nat.O)) Nat1 Nat2.
def R (n1 : fol.term Nat1) (n2 : fol.term Nat2) := fol.pred_apply REL n1 n2.

axiom_Rsurj1 : fol.proof (fol.all Nat2 (n2 => fol.ex Nat1 (n1 => R n1 n2))).
axiom_Rsurj2 : fol.proof (fol.all Nat1 (n1 => fol.ex Nat2 (n2 => R n1 n2))).
axiom_eq_morph1 : fol.proof (fol.all Nat1 (n1 => fol.all Nat2 (n2 => fol.imp (R n1 n2) (fol.all Nat1 (n1' => fol.all Nat2 (n2' => fol.imp (R n1' n2') (fol.imp (eq.eq Nat1 n1 n1') (eq.eq Nat2 n2 n2')))))))).
axiom_eq_morph2 : fol.proof (fol.all Nat2 (n2 => fol.all Nat1 (n1 => fol.imp (R n1 n2) (fol.all Nat2 (n2' => fol.all Nat1 (n1' => fol.imp (R n1' n2') (fol.imp (eq.eq Nat2 n2 n2') (eq.eq Nat1 n1 n1')))))))).
axiom_leq_morph1 : fol.proof (fol.all Nat1 (n1 => fol.all Nat2 (n2 => fol.imp (R n1 n2) (fol.all Nat1 (n1' => fol.all Nat2 (n2' => fol.imp (R n1' n2') (fol.imp (leq1 n1 n1') (leq2 n2 n2')))))))).
axiom_leq_morph2 : fol.proof (fol.all Nat2 (n2 => fol.all Nat1 (n1 => fol.imp (R n1 n2) (fol.all Nat2 (n2' => fol.all Nat1 (n1' => fol.imp (R n1' n2') (fol.imp (leq2 n2 n2') (leq1 n1 n1')))))))).
axiom_add_morph1 : fol.proof (fol.all Nat1 (n1 => fol.all Nat2 (n2 => fol.imp (R n1 n2) (fol.all Nat1 (n1' => fol.all Nat2 (n2' => fol.imp (R n1' n2') (R (add1 n1 n1') (add2 n2 n2')))))))).
axiom_add_morph2 : fol.proof (fol.all Nat2 (n2 => fol.all Nat1 (n1 => fol.imp (R n1 n2) (fol.all Nat2 (n2' => fol.all Nat1 (n1' => fol.imp (R n1' n2') (R (add1 n1 n1') (add2 n2 n2')))))))).

def commutes (A : fol.sort) (f : fol.term A -> fol.term A -> fol.term A) :=
  fol.all A (x => fol.all A (y => eq.eq A (f x y) (f y x))).

def trans (A : fol.sort) (p : fol.term A -> fol.term A -> fol.prop) :=
  fol.all A (x => fol.all A (y => fol.all A (z => fol.imp (p x y) (fol.imp (p y z) (p x z))))).
