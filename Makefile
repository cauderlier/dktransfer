
DKTACTICS=$(shell dktactics)

DKCHECK_OPTIONS=-nl -I $(DKTACTICS)/fol
DKMETA_OPTIONS=$(DKCHECK_OPTIONS) -I $(DKTACTICS)/meta

DKMETA=dkmeta
DKCHECK=dkcheck

DKMS=$(wildcard *.dkm)
DKS=$(wildcard *.dk) $(DKMS:%.dkm=%.dk)

all: $(DKS:%.dk=%.dko)

%.dko: %.dk
	$(DKCHECK) -e $(DKMETA_OPTIONS) $<

%.dk: %.dkm
	$(DKCHECK) $(DKMETA_OPTIONS) $< > $@
	$(DKMETA) $(DKMETA_OPTIONS) $< > $@


auto.dko: auto.dk

transfer_def.dk: transfer_def.dkm auto.dko

transfer_def.dko: transfer_def.dk

transfer.dko: transfer.dk transfer_def.dko

nat_def.dko: nat_def.dk

nat_transfer_setup.dko: nat_transfer_setup.dk nat_def.dko transfer_def.dko transfer.dko

nat_transfer.dk: nat_transfer.dkm auto.dko transfer_def.dko transfer.dko nat_transfer_setup.dko

nat_transfer.dko: nat_transfer.dk nat_def.dko nat_transfer_setup.dko

clean:
	rm -rf *.dko $(DKMS:.dkm=.dk)
